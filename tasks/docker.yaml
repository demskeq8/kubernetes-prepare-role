---
- name: install packages
  import_tasks: docker_DEBIAN.yaml
  when: ansible_os_family | lower == "debian"
  become: true

- name: add login user to docker group
  become: true
  user:
    name: "{{ ansible_user }}"
    groups:
      - docker
    append: yes

- name: check if main docker configuration exists
  stat:
    path: "/etc/docker/daemon.json"
  register: stat

- name: get main docker configuration
  when: stat.stat.exists == true
  block:
    - name: create temporary local file for daemon.json
      become: false
      tempfile:
        state: file
        suffix: temp
      register: tmp_docker_config
      delegate_to: '127.0.0.1'

    - name: fetch config.json
      become: true
      fetch:
        dest: "{{ tmp_docker_config.path }}"
        src: "/etc/docker/daemon.json"
        flat: "yes"

    - name: load docker config
      include_vars:
        file: "{{ tmp_docker_config.path }}"
        name: docker_daemon_config

  always:
    - name: destroy the local tmp_docker_config
      become: false
      file:
        path: "{{ tmp_docker_config.path }}"
        state: absent
      delegate_to: '127.0.0.1'

- name: configure docker kubernetes
  set_fact:
    docker_daemon_config:
      "{{ docker_daemon_config|default({}) |
          combine({'exec-opts': ['native.cgroupdriver=systemd'],
                   'log-driver': 'json-file',
                   'log-opts': {
                      'max-size': '100m'
                   },
                   'storage-driver': 'overlay2',
                   'registry-mirrors': ['https://nexus3.onap.org:10001']
                   }, recursive=True) }}"

- name: write config file
  become: true
  copy:
    dest: "/etc/docker/daemon.json"
    content: "{{ docker_daemon_config | to_nice_json(indent=2) }}"
  notify: restart docker

- name: ensure user docker config directory exists
  file:
    path: "{{ ansible_user_dir }}/.docker"
    state: directory

- name: check if docker configuration exists
  stat:
    path: "{{ ansible_user_dir }}/.docker/config.json"
  register: stat

- name: set proxy configuration to Docker
  when:
    ansible_env.HTTP_PROXY is defined or
    ansible_env.HTTPS_PROXY is defined or
    ansible_env.NO_PROXY is defined or
    ansible_env.DOCKER_MIRROR is defined
  block:
    - name: get docker config information
      when: stat.stat.exists == true
      block:
        - name: create temporary local file for config.json
          become: false
          tempfile:
            state: file
            suffix: temp
          register: tmp_docker_config
          delegate_to: '127.0.0.1'

        - name: fetch config.json
          fetch:
            dest: "{{ tmp_docker_config.path }}"
            src: "{{ ansible_user_dir }}/.docker/config.json"
            flat: "yes"

        - name: load docker config
          include_vars:
            file: "{{ tmp_docker_config.path }}"
            name: docker_config

      always:
        - name: destroy the local tmp_docker_config
          become: false
          file:
            path: "{{ tmp_docker_config.path }}"
            state: absent
          delegate_to: '127.0.0.1'

    - name: add http proxy configuration to docker_config
      set_fact:
        docker_config:
          "{{ docker_config|default({}) |
          combine({'proxies':{'default':
                    {'httpProxy': ansible_env.HTTP_PROXY }}},
                  recursive=True) }}"
      when: ansible_env.HTTP_PROXY is defined

    - name: add http proxy configuration to docker_config
      set_fact:
        docker_config:
          "{{ docker_config|default({}) |
          combine({'proxies':{'default':
                    {'httpsProxy': ansible_env.HTTPS_PROXY }}},
                  recursive=True) }}"
      when: ansible_env.HTTPS_PROXY is defined

    - name: add http proxy configuration to docker_config
      set_fact:
        docker_config:
          "{{ docker_config|default({}) |
          combine({'proxies':{'default':{'noProxy': ansible_env.NO_PROXY }}},
                  recursive=True) }}"
      when: ansible_env.NO_PROXY is defined

    - name: add docker mirror configuration to docker_config
      set_fact:
        docker_config:
          "{{ docker_config|default({}) |
          combine({'registry-mirrors': [ansible_env.DOCKER_MIRROR]},
                  recursive=True) }}"
      when: ansible_env.DOCKER_MIRROR is defined


    - name: write config file
      copy:
        dest: "{{ ansible_user_dir }}/.docker/config.json"
        content: "{{ docker_config | to_nice_json(indent=2) }}"

- name: add logrotate to docker logs
  become: true
  copy:
    dest: /etc/logrotate.d/docker-container
    content: |
      /var/lib/docker/containers/*/*.log {
        rotate {{ logrotate.number }}
        daily
        compress
        size={{ logrotate.size }}
        missingok
        delaycompress
        copytruncate
      }

- name: flush docker handlers
  meta: flush_handlers

- name: Upgrade pip
  become: true
  environment: "{{ proxy_env }}"
  pip:
    name: pip
    state: latest
  tags:
    - skip_ansible_lint

- name: install docker py and openshift
  become: true
  environment: "{{ proxy_env }}"
  pip:
    name: "{{ item }}"
    state: present
  loop:
    - docker
    - openshift
